﻿using System.IO;
using System.Linq;
using System.Reflection;
using System.Web.Http;
using Autofac;
using Autofac.Integration.WebApi;
using LiteDB;
using Microsoft.Owin;
using Microsoft.Owin.FileSystems;
using Microsoft.Owin.StaticFiles;
using Owin;
using ServiceTester.Api;
using Swashbuckle.Application;
using Topshelf;
using Topshelf.Autofac;
using TopShelf.Owin;

namespace ServiceTester
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            var container = CreateContainer();
            CreateTestData(container.Resolve<LiteDatabase>());

            HostFactory.Run(c =>
            {
                c.UseAutofacContainer(container);

                c.RunAsNetworkService();
                c.Service<TestService>(s =>
                {
                    s.ConstructUsingAutofacContainer();
                    s.WhenStarted(svc => svc.Start());
                    s.WhenStopped(svc => svc.Stop());

                    s.OwinEndpoint(app =>
                    {
                        app.Domain = "localhost";
                        app.Port = 8081;
                        app.ConfigureHttp(config =>
                        {
                            config.MapHttpAttributeRoutes(new CentralizedPrefixProvider("api"));
                            config.Formatters.Remove(config.Formatters.XmlFormatter);
                            config.Formatters.Add(config.Formatters.JsonFormatter);

                            config.EnableSwagger(x => x.SingleApiVersion("v1", "Service Tester API")).EnableSwaggerUi();
                        });

                        app.ConfigureAppBuilder(builder =>
                        {
                            var options = new FileServerOptions
                            {
                                EnableDirectoryBrowsing = true,
                                EnableDefaultFiles = true,
                                DefaultFilesOptions = { DefaultFileNames = { "index.html" } },
                                FileSystem = new PhysicalFileSystem("../../app"),
                                StaticFileOptions = { ContentTypeProvider = new CustomContentTypeProvider() }
                            };
                            builder.UseFileServer(options);

                            var srcOptions = new FileServerOptions
                            {
                                EnableDirectoryBrowsing = true,
                                EnableDefaultFiles = false,
                                RequestPath = new PathString("/src"),
                                FileSystem = new PhysicalFileSystem("../../src"),
                                StaticFileOptions = { ContentTypeProvider = new CustomContentTypeProvider() }
                            };
                            builder.UseFileServer(srcOptions);

                            builder.MapSignalR();
                        });

                        app.UseDependencyResolver(new AutofacWebApiDependencyResolver(container));
                    });
                });
            });
        }

        private static void CreateTestData(LiteDatabase db)
        {
            var dbServices = db.GetCollection<Service>("services");
            var services = Enumerable.Range(1, 6).Select(x => new Service { Name = $"Service {x}", Description = "The service for something" }).ToArray();
            dbServices.Insert(services);

            var dbTests = db.GetCollection<Test>("tests");
            foreach (var service in services)
            {
                var tests = Enumerable.Range(1, 9)
                        .Select(x => new Test
                        {
                            ServiceId = service.Id,
                            Name = $"Test {service.Id}:{x}",
                            Endpoint = (x <= 2 ? "jobs/{id}" : x <= 5 ? "connectors" : "connectors/{id}")
                        });
                dbTests.Insert(tests);
            }
        }

        private static IContainer CreateContainer()
        {
            var iocBuilder = new ContainerBuilder();
            iocBuilder.RegisterType<TestService>();

            var memory = new MemoryStream();
            iocBuilder.RegisterInstance(new LiteDatabase(memory)).SingleInstance();

            iocBuilder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            var container = iocBuilder.Build();
            return container;
        }
    }
}