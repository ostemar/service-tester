import {FrameworkConfiguration} from 'aurelia-framework';

export function configure(aurelia: FrameworkConfiguration) {
  aurelia.globalResources([
  	'./filter',
  	'./group-by',
  	'./sort',
  	'./take',
  	// './date-format', 
  	// './number-format',
  ]);
}
