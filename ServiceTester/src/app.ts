import {autoinject} from 'aurelia-framework';
import {Router, RouterConfiguration} from 'aurelia-router'
import {EventAggregator} from 'aurelia-event-aggregator';

@autoinject
export class App {
  router: Router;
  subscription: any;

  constructor(private eventAggregator: EventAggregator) {
  }

  configureRouter(config: RouterConfiguration, router: Router) {
    config.title = 'Service Tester';
    config.map([
      { route: 'services', name: 'services', moduleId: 'services/section', nav: true, title: 'Services' },
      // { route: 'services/:serviceId', name: 'service', moduleId: 'service' },
      // { route: 'services/:serviceId/tests/:testId', name: 'test', moduleId: 'test' },
      { route: '', redirect: 'services' }
    ]);

    this.router = router;
  }

   navigationSuccess(event) {
    let instruction = event.instruction;    
    console.log(instruction);
  }

  attached() {
    this.subscription = this.eventAggregator.subscribe(
      'router:navigation:success',
      this.navigationSuccess.bind(this));
  }

  detached() {
    this.subscription.dispose();
  }
}
