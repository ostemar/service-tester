import {autoinject} from 'aurelia-framework';
import {HttpClient} from 'aurelia-fetch-client';
import 'fetch';
import {Router, RouterConfiguration} from 'aurelia-router'

@autoinject
export class Services {
  services = [];

  constructor(private http: HttpClient, private router: Router) {
    http.configure(config => {
      config
        .useStandardConfiguration()
        .withBaseUrl('api/');
    });
  }

  activate() {
    return this.http.fetch('services')
      .then(response => response.json())
      .then(services => this.services = services);
  }

  gotoService(id) {
    this.router.navigate(`${id}`);
  }

  runTests(id) {
    alert("Run Tests");    
  }
}
