import {autoinject} from 'aurelia-framework';
import {HttpClient} from 'aurelia-fetch-client';
import 'fetch';
import {Router} from 'aurelia-router';

@autoinject
export class Service {
  service = null;
  tests = []
  serviceId = null;
  
  constructor(private http: HttpClient, private router: Router) {
    http.configure(config => {
      config
      .useStandardConfiguration()
      .withBaseUrl('api/');
    });
  }

  activate(params, routeConfig) {
    this.serviceId = params.serviceId;
    return Promise.all(
      [this.http.fetch(`services/${params.serviceId}`)
      .then(response => response.json())
      .then(service => this.service = service)
      .then(() => {
        routeConfig.navModel.setTitle(this.service.Name);
        routeConfig.title = this.service.Name;
      }),
      this.http.fetch(`services/${params.serviceId}/tests`)
      .then(response => response.json())
      .then(tests => this.tests = tests)
      ]);
  }

  gotoTest(id) {
    this.router.navigate(`${this.serviceId}/tests/${id}`);
  }
}
