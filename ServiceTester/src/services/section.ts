import {autoinject} from 'aurelia-framework';
import {Router, RouterConfiguration} from 'aurelia-router'

@autoinject
export class Section {
  router: Router;

  configureRouter(config, router) {
        config.map([
            { route: '', moduleId: './list', nav: false },
            { route: ':serviceId', name: 'service', moduleId: './service', nav: false, title: "Service" },
            { route: ':serviceId/tests/:testId', name: 'test', moduleId: '../tests/section', nav: false, title: "Test" }
        ]);
        this.router = router;
    };
}
