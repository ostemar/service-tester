import {autoinject} from 'aurelia-framework';
import {Router, RouterConfiguration} from 'aurelia-router'

@autoinject
export class Section {
  router: Router;

  configureRouter(config, router) {
        config.map([
            { route: '', moduleId: './test', nav: false },
        ]);
        this.router = router;
    };
}
