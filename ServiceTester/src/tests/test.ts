import {autoinject} from 'aurelia-framework';
import {HttpClient} from 'aurelia-fetch-client';
import 'fetch';
import {Router} from 'aurelia-router';

@autoinject
export class Test {
  test = null;
  service = null;

  constructor(private http: HttpClient, private router: Router) {
    http.configure(config => {
      config
        .useStandardConfiguration()
        .withBaseUrl('/api/');
    });
  }

  activate(params, routeConfig) {
    console.log("WJAAT");
    return Promise.all([
      this.http.fetch(`tests/${params.testId}`)
        .then(response => response.json())
        .then(test => this.test = test)
        .then(() => {
          routeConfig.navModel.setTitle(this.test.Name);
          routeConfig.title = this.test.Name;
        }),
      this.http.fetch(`services/${params.serviceId}`)
        .then(response => response.json())
        .then(service => this.service = service),
       ]);
  }
}
