using System;
using System.Timers;
using Microsoft.AspNet.SignalR.Client;
using TopShelf.Owin;

namespace ServiceTester
{
    public class TestService : IOwinService
    {
        readonly Timer _timer;
        private IHubProxy _hub;

        public TestService()
        {
            _timer = new Timer(5000) { AutoReset = true };
            _timer.Elapsed += (sender, eventArgs) =>
            {
                var message = $"It is {DateTime.Now} and all is well";
                Console.WriteLine(message);
                _hub.Invoke("Send", "Service", message);
            };
        }

        public bool Start()
        {
            var conn = new HubConnection("http://localhost:8081");
            _hub = conn.CreateHubProxy("TestHub");
            conn.Start().Wait();

            _timer.Start();
            return true;
        }

        public bool Stop()
        {
            _timer.Stop();
            return true;
        }
    }
}