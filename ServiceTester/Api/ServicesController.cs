﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;
using LiteDB;

namespace ServiceTester.Api
{
    [RoutePrefix("services")]
    public class ServicesController : ApiController
    {
        private readonly LiteDatabase _db;

        public ServicesController(LiteDatabase db)
        {
            _db = db;
        }

        /// <summary>
        /// Get a specific service.
        /// </summary>
        /// <param name="id">The id of the service.</param>
        /// <returns>The service specification.</returns>
        [HttpGet, Route("{id}")]
        [ResponseType(typeof(Service))]
        public IHttpActionResult Get(int id)
        {
            var service = _db.GetCollection<Service>("services").FindOne(x => x.Id == id);
            if (service != null)
                return Ok(service);

            return NotFound();
        }

        /// <summary>
        /// Gets all services.
        /// </summary>
        /// <returns>A list of all services.</returns>
        [HttpGet, Route("")]
        public IEnumerable<Service> GetAll()
        {
            var services = _db.GetCollection<Service>("services");
            return services.FindAll();
        }
    }

    public class Service
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}