﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;
using LiteDB;

namespace ServiceTester.Api
{
    [RoutePrefix("tests")]
    public class TestsController : ApiController
    {
        private readonly LiteDatabase _db;

        public TestsController(LiteDatabase db)
        {
            _db = db;
        }

        /// <summary>
        /// Get a specific test.
        /// </summary>
        /// <param name="id">The id of the test.</param>
        /// <returns>The test specification</returns>
        [HttpGet, Route("{id}")]
        [ResponseType(typeof(Test))]
        public IHttpActionResult Get(int id)
        {
            var test = _db.GetCollection<Test>("tests").FindOne(x => x.Id == id);
            if (test != null)
                return Ok(test);

            return NotFound();
        }

        /// <summary>
        /// Gets all tests.
        /// </summary>
        /// <returns>A list of all tests.</returns>
        [HttpGet, Route("")]
        public IEnumerable<Test> GetAll()
        {
            return _db.GetCollection<Test>("tests").FindAll();
        }

        /// <summary>
        /// Gets all tests for a specific service.
        /// </summary>
        /// <param name="serviceId">The id of the service.</param>
        /// <returns>A list of all tests for the specified service.</returns>
        [HttpGet, Route("~/api/services/{serviceId}/tests")]
        public IEnumerable<Test> GetAllForService(int serviceId)
        {
            return _db.GetCollection<Test>("tests").Find(x => x.ServiceId == serviceId);
        }
    }

    public class Test
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Endpoint { get; set; } = "/jobs/{id}";
        public int ServiceId { get; set; }
    }
}
