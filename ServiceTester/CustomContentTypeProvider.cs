using Microsoft.Owin.StaticFiles.ContentTypes;

namespace ServiceTester
{
    public class CustomContentTypeProvider : FileExtensionContentTypeProvider
    {
        public CustomContentTypeProvider()
        {
            Mappings.Add(".json", "application/json");
            Mappings.Remove(".ts"); // Remove some video in strange encoding occupying TypeScripts suffix
            Mappings.Add(".ts", "application/x-typescript");
        }
    }
}