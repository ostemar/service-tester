using Microsoft.AspNet.SignalR;

namespace ServiceTester
{
    public class TestHub : Hub
    {
        public void Send(string name, string message)
        {
            Clients.All.addMessage(name, message);
        }
    }
}